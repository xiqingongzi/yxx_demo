from django.contrib.auth.forms import UserCreationForm
from .models import User

from django.forms import ModelForm


class RegisterForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ("username", "email")
class ProfileForm(ModelForm):
    class Meta:
        model = User
        fields = ['nickname','bio','website','github','weibo']