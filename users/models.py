from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    nickname = models.CharField("昵称",max_length=50, blank=True)
    bio = models.TextField("个人介绍",blank=True)
    website = models.URLField("网站地址",help_text="粘贴您的网站链接至此",blank=True)
    github = models.URLField("Github 地址",help_text="粘贴您的个人页面链接至此",blank=True)
    weibo = models.URLField("微博地址",help_text="粘贴您的微博至此",blank=True)
    class Meta(AbstractUser.Meta):
        pass