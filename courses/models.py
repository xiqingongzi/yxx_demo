from django.db import models
from simplemde.fields import SimpleMDEField
import markdown
from django.utils.html import strip_tags
from django.conf import settings


""" 状态 """
STATUS_CHOICES = (
    ('d', '草稿'),
    ('p', '已发布'),
    ('w', '已删除'),
)

class Category(models.Model):
    title = models.CharField('标题',max_length=100)
    description = SimpleMDEField(verbose_name=u'描述')
    excerpt = models.CharField("摘要",max_length=200, blank=True)
    image = models.ImageField('图片',upload_to="pictures/%Y/%m/%d",help_text="比例为16:9")
    pub_date = models.DateTimeField('发布日期',auto_now_add=True)
    update_date = models.DateTimeField('更新日期',auto_now=True)
    sort = models.IntegerField("序号",default=99,help_text="序号越小越靠前")
    def save(self, *args, **kwargs):
        if not self.excerpt:
             md = markdown.Markdown(extensions=[
                'markdown.extensions.extra',
                'markdown.extensions.codehilite',
            ])
             self.excerpt = strip_tags(md.convert(self.description))[:54]
        super(Category, self).save(*args, **kwargs)
    def __unicode__(self):
        return self.title
    def __str__(self):
        return self.title
    class Meta:
        verbose_name = '目录'
        verbose_name_plural = '0-目录'
        ordering = ['sort']  # 按照哪个栏目排序
        get_latest_by = 'pub_date'

class Series(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    category = models.ForeignKey(Category)
    title = models.CharField('标题',max_length=100)
    description = SimpleMDEField(verbose_name=u'描述')
    excerpt = models.CharField("摘要",max_length=200, blank=True)
    image = models.ImageField('图片',upload_to="pictures/%Y/%m/%d",help_text="比例为16:9")
    pub_date = models.DateTimeField('发布日期',auto_now_add=True)
    update_date = models.DateTimeField('更新日期',auto_now=True)
    views = models.PositiveIntegerField(default=0)
    videolength = models.PositiveIntegerField(default=0)
    sort = models.IntegerField("序号",default=99,help_text="序号越小越靠前")
    status = models.CharField('状态',max_length=1, choices=STATUS_CHOICES,default='d')
    def save(self, *args, **kwargs):
        if not self.excerpt:
             md = markdown.Markdown(extensions=[
                'markdown.extensions.extra',
                'markdown.extensions.codehilite',
            ])
             self.excerpt = strip_tags(md.convert(self.description))[:54]
        super(Series, self).save(*args, **kwargs)
    def increase_views(self):
        self.views += 1
        self.save(update_fields=['views'])
    def addlength(self,length):
        self.videolength += length
        self.save(update_fields=['videolength'])
    def __unicode__(self):
        return self.title
    def __str__(self):
        return self.title
    class Meta:
        verbose_name = '课程'
        verbose_name_plural = '1-课程'
        ordering = ['sort']  # 按照哪个栏目排序
        get_latest_by = 'pub_date'

class Section(models.Model):
    category = models.ForeignKey(Category)
    series = models.ForeignKey(Series)
    title = models.CharField('标题',max_length=300)
    sort = models.IntegerField("序号",default=1,help_text="序号越小越靠前")
    pub_date = models.DateTimeField('发布日期',auto_now_add=True)
    update_date = models.DateTimeField('更新日期',auto_now=True)
    def save(self, *args, **kwargs):
        self.category_id = self.series.category_id
        super(Section, self).save(*args, **kwargs)
    def __str__(self):
        return self.title
    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name = '章节'
        verbose_name_plural = '2-章节'
        ordering = ['sort']  # 按照哪个栏目排序
        get_latest_by = 'pub_date'

class Episode(models.Model):
    category = models.ForeignKey(Category)
    series = models.ForeignKey(Series)
    section = models.ForeignKey(Section)
    title = models.CharField('标题',max_length=300)
    description = SimpleMDEField(verbose_name=u'描述')
    excerpt = models.CharField("摘要",max_length=200, blank=True)
    sort = models.IntegerField("序号",default=1,help_text="序号越小越靠前")
    video = models.FileField('附件文件',upload_to="wp-content/uploads/attachment/%Y/%m/%d")
    video_length = models.PositiveIntegerField("视频长度",help_text='以秒为单位')
    pub_date = models.DateTimeField('发布日期',auto_now_add=True)
    update_date = models.DateTimeField('更新日期',auto_now=True)
    status = models.CharField('状态',max_length=1, choices=STATUS_CHOICES,default='d')
    def save(self, *args, **kwargs):
       
        self.series = self.section.series
        if not self.excerpt:
            md = markdown.Markdown(extensions=[
                'markdown.extensions.extra',
                'markdown.extensions.codehilite',
            ])
            self.excerpt = strip_tags(md.convert(self.description))[:54]
            self.series.addlength(self.video_length)
       
        self.category = self.section.category
        super(Episode, self).save(*args, **kwargs)
    def __str__(self):
        return self.title
    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name = '小节'
        verbose_name_plural = '3-小节'
        ordering = ['sort','-pub_date']  # 按照哪个栏目排序
        get_latest_by = 'pub_date'

class Attachment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    category = models.ForeignKey(Category)
    series = models.ForeignKey(Series)
    title = models.CharField('标题',max_length=100)
    description = SimpleMDEField(verbose_name=u'描述')
    excerpt = models.CharField("摘要",max_length=200, blank=True)
    file = models.FileField('附件文件',upload_to="wp-content/uploads/attachment/%Y/%m/%d")
    sort = models.IntegerField("序号",default=1,help_text="序号越小越靠前")
    pub_date = models.DateTimeField('发布日期',auto_now_add=True)
    update_date = models.DateTimeField('更新日期',auto_now=True)
    def save(self, *args, **kwargs):
        if not self.excerpt:
            md = markdown.Markdown(extensions=[
                'markdown.extensions.extra',
                'markdown.extensions.codehilite',
            ])
            self.excerpt = strip_tags(md.convert(self.description))[:54]
        self.category = self.series.category
        super(Attachment, self).save(*args, **kwargs)
    def __str__(self):
        return self.title
    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name = '附件'
        verbose_name_plural = '4-课程资料'
        ordering = ['sort','-pub_date']  # 按照哪个栏目排序
        get_latest_by = 'pub_date'
class Cite(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    category = models.ForeignKey(Category)
    series = models.ForeignKey(Series)
    title = models.CharField('标题',max_length=100)
    description = SimpleMDEField(verbose_name=u'描述')
    excerpt = models.CharField("摘要",max_length=200, blank=True)
    link = models.URLField('链接')
    sort = models.IntegerField("序号",default=1,help_text="序号越小越靠前")
    pub_date = models.DateTimeField('发布日期',auto_now_add=True)
    update_date = models.DateTimeField('更新日期',auto_now=True)
    def save(self, *args, **kwargs):
        if not self.excerpt:
            md = markdown.Markdown(extensions=[
                'markdown.extensions.extra',
                'markdown.extensions.codehilite',
            ])
            self.excerpt = strip_tags(md.convert(self.description))[:54]
        self.category = self.series.category
        super(Cite, self).save(*args, **kwargs)
    def __str__(self):
        return self.title
    def __unicode__(self):
        return self.title
    class Meta:
        verbose_name = '延展阅读'
        verbose_name_plural = '5-延展阅读'
        ordering = ['sort','-pub_date']  # 按照哪个栏目排序
        get_latest_by = 'pub_date'