from django.shortcuts import render
from .models import * 
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def indexView(request):
    return render(request,"index.html")

def categoriesView(request):
    categories_list = Category.objects.all()

    paginator = Paginator(categories_list, 25) # Show 25 contacts per page

    page = request.GET.get('page')

    try:
        categories = paginator.page(page)
    except PageNotAnInteger :
        categories = paginator.page(1)
    except EmptyPage:
        categories = paginator.page(paginator.num_pages)

    return render(request,"category.html",{"categories":categories})

def categoryView(request,category_id):
    category = Category.objects.get(pk = category_id)
    category.description = markdown.markdown(category.description)
    series_list=  Series.objects.filter(category_id=category_id,status="p")
    paginator = Paginator(series_list, 25) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        series = paginator.page(page)
    except PageNotAnInteger :
        series = paginator.page(1)
    except EmptyPage:
        series = paginator.page(paginator.num_pages)

    return render(request,"series_list.html",{"series":series,"category":category})

def seriesView(request,series_id):
    series = Series.objects.get(pk = series_id)
    sections = Section.objects.filter(series_id = series_id)
    episodes = Episode.objects.filter(series_id = series_id,status="p")
    attachments = Attachment.objects.filter(series_id = series_id )
    cites = Cite.objects.filter(series_id = series_id )

    series.description = markdown.markdown(series.description,extensions=[
                                     'markdown.extensions.extra',
                                     'markdown.extensions.codehilite',
                                     'markdown.extensions.toc',
                                  ])
    return render(request,"series.html",{"series":series,
                                        "sections":sections,
                                        "episodes":episodes,
                                        "attachments":attachments,
                                        "cites":cites
                                        })                

def episodeView(request,episode_id):
    episode = Episode.objects.get(pk=episode_id)
    episode.description = markdown.markdown(episode.description,extensions=[
                                     'markdown.extensions.extra',
                                     'markdown.extensions.codehilite',
                                     'markdown.extensions.toc',
                                  ])
    return render(request,"episode.html",{"episode":episode})