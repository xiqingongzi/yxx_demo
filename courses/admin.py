from django.contrib import admin
from django.contrib.admin import AdminSite

from .models import *

"""
后台 title 设置
"""
admin.site.site_header = '云学院管理后台'
admin.site.site_title = '云学院'

'''
发布功能定义
'''
def make_published(modeladmin, request, queryset):
    queryset.update(status='p')
make_published.short_description = "将选中课程标记为已发布"
def make_draft(modeladmin, request, queryset):
    queryset.update(status='d')
make_draft.short_description = "将选中课程标记为草稿"
def make_withdrawn(modeladmin, request, queryset):
    queryset.update(status='w')
make_withdrawn.short_description = "将选中课程标记为回收站"


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id','title', 'excerpt','pub_date','sort')
    search_fields = ['title','description']
    readonly_fields =['pub_date','update_date']
    list_display_links = ('id','title','excerpt')
    list_per_page = 50
    fieldsets = (
        ('基础信息', {
            'fields': ('title', 'description','sort')
        }),
         ('其他信息', {
            'fields': ('image','pub_date','update_date')
        }),
    )
    pass

class SeriesAdmin(admin.ModelAdmin):
    list_display = ('id','category','user','title', 'excerpt','pub_date','status')
    list_filter = ['pub_date']
    search_fields = ['title','description']
    list_per_page = 50
    fk_fields=['category']
    list_display_links = ('id','status','title','excerpt')
    fieldsets = (
        ('基础信息', {
            'fields': ('category','user','title', 'description','image',)
        }),
        ('其他信息', {
            'fields': ('status','views','pub_date','update_date' ),
        }),
    )
    readonly_fields = ['pub_date','update_date','views']
    actions = [make_published,make_draft,make_withdrawn]

class SectionAdmin(admin.ModelAdmin):
    list_display = ('id','category','series','title','sort')
    list_filter = ['pub_date']
    search_fields = ['title','sort']
    list_per_page = 50
    fk_fields=['series']
    list_display_links = ('id','title','sort')
    fieldsets = (
        ('基础信息', {
            'fields': ('series','title', 'sort',)
        }),
    )
    readonly_fields = ['pub_date','update_date']  

class EpisodeAdmin(admin.ModelAdmin):
    list_display = ('id','section','title', 'excerpt','pub_date','status')
    list_filter = ['pub_date']
    search_fields = ['title','description']
    list_per_page = 50
    fk_fields=['series']
    list_display_links = ('id','status','title','excerpt')
    fieldsets = (
        ('基础信息', {
            'fields': ('section','title', 'description','sort','status',)
        }),
        ('视频信息', {
            'fields': ('video','video_length')
        }),     
    )
    readonly_fields = ['pub_date','update_date']
    actions = [make_published,make_draft,make_withdrawn]
    pass

class AttachmentAdmin(admin.ModelAdmin):
    list_display = ('id','series','title', 'excerpt','pub_date')
    list_filter = ['pub_date']
    search_fields = ['title','excerpt']
    list_per_page = 50
    fk_fields=['series']
    list_display_links = ('id','title','excerpt','pub_date')
    fieldsets = (
        ('基础信息', {
            'fields': ('series','user','title', 'description','sort',)
        }),
        ('文件信息', {
            'fields': ('file',)
        }),     
    )
    readonly_fields = ['pub_date','update_date']

class CiteAdmin(admin.ModelAdmin):
    list_display = ('id','series','title', 'excerpt','pub_date')
    list_filter = ['pub_date']
    search_fields = ['title','excerpt']
    list_per_page = 50
    fk_fields=['series']
    list_display_links = ('id','title','excerpt','pub_date')
    fieldsets = (
        ('基础信息', {
            'fields': ('series','user','title', 'description','sort',)
        }),
        ('文件信息', {
            'fields': ('link',)
        }),     
    )
    readonly_fields = ['pub_date','update_date']

admin.site.register(Category,CategoryAdmin)
admin.site.register(Series,SeriesAdmin)
admin.site.register(Section,SectionAdmin)
admin.site.register(Episode,EpisodeAdmin)
admin.site.register(Attachment,AttachmentAdmin)
admin.site.register(Cite,CiteAdmin)