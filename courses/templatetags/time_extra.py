from django import template

register = template.Library()

@register.filter
def time_format(value):
    hour = int(value / 3600)
    minutes  = int(value % 3600 / 60)
    seconds  = int(value % 3600 % 60)
    if hour != 0 :
        return "%s 小时 %s 分钟 %s 秒"  %(hour,minutes,seconds)
    else:
        return "%s 分钟 %s 秒"  %(minutes,seconds)
        pass
