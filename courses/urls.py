from django.conf.urls import url

from .views import * 

urlpatterns = [
    url(r"^$",categoriesView,name="index"),
    url(r'^category/(?P<category_id>[0-9]+)',categoryView,name="category"),
    url(r'^series/(?P<series_id>[0-9]+)',seriesView,name="series"),
    url(r'^episode/(?P<episode_id>[0-9]+)',episodeView,name="episode"),
]